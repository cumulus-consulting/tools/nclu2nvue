# nclu2nvue

NCLU to NVUE migration script. This script supports migration upto Cumulus Linux 5.3

## NCLU Commands

All NCLU commands are stored in [./tests/nclu/list-commands.txt](./tests/nclu/list-commands.txt).

## Making Changes

Describe the add/change/delete process and the corresponding changes to tests

## Deployment/Release Process

Currently the script runs in a container deployed in AIR at https://air.nvidia.com/migrate/. In order to release a new version you need to:

1. Create a new branch and push the required changes into it.
2. Make sure Gitlab-CI completes and all tests are green.
3. Merge the PR into the `main` branch.

For all logs from production and any questions about the AIR CI pipline contact Nick Mitchell.

## Run it as a standalone script

In order to run the migration script as a standalone script
1. Extract the `nclu.commands` file from the cl_support file.
    - Unzip the cl_support file.
    - Navigate to `Support/` and find the `nclu.commands` file.
2. Run the `convert.py` against the nclu.commands file.<br>
    `$ python3 convert.py -i nclu.commands`
3. By default, the output configuration is stored in a file named `output.txt`. If you would like to change this, specify the name with the `-o` option.<br>
    `$ python3 convert.py -i nclu.commands -o nvue-output.txt`
