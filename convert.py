#!/bin/env python3.7
# Start date 24/05/2021
# Convert4-5 python script
# Version 0.3 / Dated 27/05/2021 / bad coding by Michael Schipp
# Version 0.4 / Dated 06/06/2021 / added net add dns and net add syslog
# Version 0.5 / Dated 06/06/2021 / fixes dns,ntp etc
# Version 0.6 / Dated 04/08/2021 / refactor code, finished up bgp and interfaces
# Version 0.6.1 / Dated 05/08/2021 / Fixed debug mode, filtered static nat
# Version 0.7 / Dated 24/08/2021 / Fixed bond bugs, pulled in merges for VXLAN and VNI
# Version 0.7.1 / Dated 25/08/2021 / Fixed post-up and not supported commands
# Version 0.7.2 / Dated 31/08/2021 / Fixed post-up and not supported commands
# Version 0.7.3 / Dated 01/09/2021 / L2VPN EVPN IPv4 advertise unicast
# Version 0.8 / Dated 22/09/2021 / DHCP Relay, Breakouts, MLAG backup IP, new comments/tags
# Version 0.9 / Dated 22/10/2021 / refactoring Hostname and BGP for CL5.0 restructure
# Version 1.0 / Dated 20/1/2022 / Fixing BGP static networking. Renamed variable fron CUE to NVUE
# Version 1.1 / Dated 21/1/2022 / Fixing merge typos, and hostname logic
# Version 1.2 / Dated 21/1/2022 / Fixing hostname logic
# Version 1.3 / Dated 23/1/2023 / Adding support for CL 5.2/5.3, NTP source, timezone and additional commands - Updated by Krishna Vasudevan
# Version 1.3.1 / Dated 13/02/2023 / Adding support for ACL - Updated by Krishna Vasudevan
# Version 1.4 / Dated 15/02/2023 / Adding support for CL 5.4 - Updated by Krishna Vasudevan
# Version 1.5 / Dated 01/06/2023 / Adding support for CL 5.5 - Updated by Krishna Vasudevan
# Version 1.5.1 / Dated 19/06/2023 / Bug fixes, support for additional route-map configurations - Updated by Krishna Vasudevan
# Version 1.6 / Dated 21/09/2023 / Bug fixes, updated interface name - Updated by Krishna Vasudevan
# Version 1.7 / Dated 15/01/2024 / Bug fixes, updates to OSPF configuration, support for CL 5.7, prefix lists - Updated by Krishna Vasudevan
# Version 1.9 / Dated 15/05/2024 / Bug fixes - Updated by Krishna Vasudevan
# Version 1.10 / Dated 22/10/2024 / Bug fixes - Updated by Krishna Vasudevan

# Unsupported configs:
# ptp features beyond priority and domain-number. Most ptp features are the default state and not configurable with NVUE
# all names - replace with _
# nv set interface peerlink.4094 clag args --redirectEnable

### COMMENTS
# Any line that is not converted will be addressed with one of the following tags:
#  # FUTURE SUPPORT - Feature coming to NVUE in the future
#  # UNREQUIRED - Command no longer necessary in NVUE
#  # MANUAL REVIEW - Requires manual review of command before entry, it may not have been captured by the script
#  # SCRIPT UNSUPPORTED - Script doesn't handle input, and most likely not a relevant command in NVUE



import sys, getopt, subprocess
import re
from ipaddress import ip_address, IPv4Address 
import random

def validIPAddress(IP: str) -> str: 
    try: 
        return "IPv4" if type(ip_address(IP.split('/')[0])) is IPv4Address else "IPv6"
    except ValueError: 
        return "Invalid"


# set global variable
current_ver = '1.10'
NCLU = 'net'
NVUE = 'nv'
NVUE_SET  = NVUE + ' set '
NVUE_UNSET = NVUE + ' unset '
DEBUG = False
inputfile = '\n'
outputfile = 'output.txt'

# L2 VNI struct -- maps VNI interfaces to VXLAN ID and VLAN ID
VNI_STRUCT = {
  'dummy': {
    'vni': '5010',
    'vlan': '10'
  }
}


# L3 VNI set -- a set of VLAN IDs for L3 VNIs for special treatment
# It relies on the fact that 'net add vrf RED vni' comes before 'net add vlan'
L3_VNIs = set()


# Helper function that runs a shell command and returns its output as string
def run_shell_cmd(cmd):
  r = subprocess.run(cmd, capture_output=True, shell=True, text=True)
  return r.stdout

# Checks systemd for a particular process p and returns the vrf name as string
def check_vrf(p):
  cmd = f'systemctl --no-wall --no-legend --no-pager list-units {p}*'
  result = run_shell_cmd(cmd)
  parts = result.split('@')
  if len(parts) == 2:
     #['ntp.server', 'mgmt.service']
    return parts[1].split('.')[0]
  if len(parts) == 1:
    return 'default'
  else:
     raise Exception(f'unexpected systemd unit format')


UNIQUE_PREFIX_STRINGS = set()
def handle_prefix_string(pstr, str_list):
  if not pstr in UNIQUE_PREFIX_STRINGS:
     UNIQUE_PREFIX_STRINGS.add(pstr)
     str_list.append(pstr)



INTERFACE_LINK_STATES = dict()
def process_interfaces(line_str):
  temp_list = line_str.split()

  add_del = temp_list[1]
  int_type = temp_list[2]
  # Added in 1.6 - Interface name - updated to accomodate hyphens in the name - e.g.: swp1-11,eth1-3,port-channel32-40, xn03-101-prod, 'peerlink,port-channel1-12,17-20,25-28,33,35,37,39,41-44,96'
  # int_name = re.sub("\-([a-zA-Z]+)", r"_\1", temp_list[3])  # replace only the string part of the hyphenated string with _
  # Step 1 - split on commas
  int_name_1 = temp_list[3].split(',')
  int_name_list = []
  # Step 2 - split at the last hyphen
  for name in int_name_1:
    int_name_2 = name.rsplit("-",1)
    # Step 3 - replace all hyphens on first part of the split
    int_name_3 = int_name_2[0].replace("-","_")
    # Step 4 - Join the strings back with hyphen
    if(len(int_name_2) > 1):
      if(int_name_2[1].isdigit()):
        int_name_4 = '-'.join([int_name_3,int_name_2[1]])
      else:
        int_name_4 = '_'.join([int_name_3,int_name_2[1]])
    else:
        int_name_4 = int_name_3
    int_name_list.append(int_name_4)
  int_name = (',').join(int_name_list)
  suffix_list = temp_list[4:]
  suffix_string = ' '.join(suffix_list)

  output_string = ''

  if int_type == 'vlan':
    int_nvue_name = 'vlan' + int_name
  else:
    int_nvue_name = int_name


  #print(suffix_string)
  ##########################################################################################
  ##### Unsupported
  if suffix_string.find('up \'') != -1 or suffix_string.find('down \'') != -1:
    return '# MANUAL REVIEW - ' + line_str

  if INTERFACE_LINK_STATES.get(int_nvue_name) == None:
    INTERFACE_LINK_STATES[int_nvue_name] = 'up'
  elif suffix_string.find('link up') != -1:
    INTERFACE_LINK_STATES[int_nvue_name] = 'up'
  elif suffix_string.find('link down') != -1:
    INTERFACE_LINK_STATES[int_nvue_name] = 'down'

  ##### Just a definition
  if len(suffix_list) == 0:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name

  #### backup ip VRF yes
  elif int_name.find('peerlink') != -1 and suffix_string.find('clag backup-ip') != -1 and suffix_string.find('vrf mgmt') != -1:
    output_string = output_string + NVUE_SET + 'mlag backup ' + suffix_list[-3] + ' vrf mgmt' + '\n'

  #### backup ip VRF no
  elif int_name.find('peerlink') != -1 and suffix_string.find('clag backup-ip') != -1 and suffix_string.find('vrf mgmt') == -1:
    output_string = output_string + NVUE_SET + 'mlag backup ' + suffix_list[-1] + '\n'

  #### peerlink ip address
  elif int_name.find('peerlink') != -1 and suffix_string.find('ip address') !=- 1:
    output_string = '# UNREQUIRED - ' + line_str

  #### peer-ip
  elif int_name.find('peerlink') != -1 and suffix_string.find('clag peer-ip') != -1:
    # Moving code to mandatory use MLAG link local
    output_string = output_string + NVUE_SET + 'mlag peer-ip linklocal\n'

  #### priority
  elif suffix_string.find('clag priority') != -1:
    output_string = output_string + NVUE_SET + 'mlag priority ' + suffix_list[-1] + '\n'

  #### sys-mac
  elif suffix_string.find('clag sys-mac')!= -1:
    output_string = output_string + NVUE_SET + 'mlag mac-address ' + suffix_list[-1] + '\n'

  #### args initDelay
  elif suffix_string.find('initDelay') != -1:
    output_string = output_string + NVUE_SET + 'mlag init-delay ' + suffix_list[-1] + '\n'

  #### args initDelay
  elif suffix_string.find('redirectEnable') != -1:
    return ""

  ##### Bond definition
  elif suffix_list[0] == 'bond':
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' type bond' + '\n'
    ##### bond members
    # net add bond bond31 bond slaves swp31
    if suffix_string.find('bond slaves') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' bond member ' + suffix_list[-1] + '\n'
      ##### bond lacp-bypass-allow
    elif suffix_string.find('bond lacp-bypass-allow') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' '
      output_string = output_string + suffix_string.replace('lacp-bypass-allow', 'lacp-bypass on') + '\n'

  ##### CLAG/MLAG
  elif suffix_string.find('clag id') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' bond mlag id ' + suffix_list[-1] + '\n'

  ### link speed / net add interface <interface> link speed (10|100|10000|25000|40000|50000|100000)
  elif suffix_string.startswith('link speed'):
    zeroesPos = suffix_list[-1].rfind('000')
    speed = suffix_list[-1] if zeroesPos == -1 else suffix_list[-1][:zeroesPos] + 'G' + suffix_list[-1][zeroesPos + len('000'):]
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' link speed ' + speed

  ##### breakout
  # Modified with CL 5.4 since the format has been changed
  elif suffix_string.find('breakout') != -1:
    # output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' link breakout ' + suffix_list[-1]
    nclu_breakout = suffix_list[-1]
    if nclu_breakout.find('x') > 0 and nclu_breakout.find('1x') < 0: 
      # catch net add interface <int> breakout disabled|loopback
      # net add interface <int> 1x - does not split the port
      breakout = nclu_breakout.split('x')[0] + "x" # Eg: 4x10G
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' link breakout ' + breakout + '\n'
      if int_nvue_name.find(',') > 0:
        # Eg: swp1-2,4,6-7,9-10,12,14
        interfaces = int_nvue_name.split(',')
        breakout_int_prefix = re.sub("(\d)|(-)|(,)","",interfaces[0])
        #interfaces[0] = re.sub("[a-zA-Z]+","",interfaces[0])
        for interface in interfaces:  
          int_nvue_name = breakout_int_prefix + re.sub("([a-zA-Z]+)","",interface)
          if int_nvue_name.find('-') > 0:
          # Eg: swp25-42
            int_range = int_nvue_name.split('-')
            range_start = int(re.sub("\D","",int_range[0]))
            range_end = int(int_range[1]) + 1
            for i in range(range_start,range_end):
              # if int(nclu_breakout.split('x')[0]) > 1:
              breakout_interfaces = "s0-" + str(int(nclu_breakout.split('x')[0])-1)
              # else:
              # breakout_interfaces = "s0"
              output_string = output_string + NVUE_SET + 'interface ' + breakout_int_prefix + str(i) + breakout_interfaces + ' link state up\n'
              if nclu_breakout.split('x')[1] != '':
                output_string = output_string + NVUE_SET + 'interface ' + breakout_int_prefix + str(i) + breakout_interfaces + ' link speed ' + nclu_breakout.split('x')[1] + '\n'
          else:
            if int(nclu_breakout.split('x')[0]) > 1:
              # breakout 2x | 4x | 8x
              breakout_interfaces = "s0-" + str(int(nclu_breakout.split('x')[0])-1)
            # else:
              # breakout 1x
              # breakout_interfaces = "s0"
            output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + breakout_interfaces + ' link state up\n'
            if nclu_breakout.split('x')[1] != '':
              # breakout 2x10G v/s breakout 2x
              output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + breakout_interfaces + ' link speed ' + nclu_breakout.split('x')[1] + '\n'
      elif int_nvue_name.find('-') > 0:
        # Eg: swp25-42
        int_range = int_nvue_name.split('-')
        breakout_int_prefix = re.sub("\d","",int_range[0])
        range_start = int(re.sub("\D","",int_range[0]))
        range_end = int(int_range[1]) + 1
        for i in range(range_start,range_end):
          if int(nclu_breakout.split('x')[0]) > 1:
            breakout_interfaces = "s0-" + str(int(nclu_breakout.split('x')[0])-1)
          # else:
          # breakout_interfaces = "s0"
          output_string = output_string + NVUE_SET + 'interface ' + breakout_int_prefix + str(i) + breakout_interfaces + ' link state up\n'
          if nclu_breakout.split('x')[1] != '':
            output_string = output_string + NVUE_SET + 'interface ' + breakout_int_prefix + str(i) + breakout_interfaces + ' link speed ' + nclu_breakout.split('x')[1] + '\n'
      else:
        if int(nclu_breakout.split('x')[0]) > 1:
          breakout_interfaces = "s0-" + str(int(nclu_breakout.split('x')[0])-1)
        # else:
        # breakout_interfaces = "s0"
        output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + breakout_interfaces + ' link state up\n'
        if nclu_breakout.split('x')[1] != '':
          output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + breakout_interfaces + ' link speed ' + nclu_breakout.split('x')[1] + '\n'
    else:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' link breakout ' + nclu_breakout + '\n'

  ##### net add interface swp1 alias xxx
  elif suffix_string.find('alias') != -1:
    description_string = ' '.join(suffix_list[1:]).replace('-','_').replace(':','_') 
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' description \'' + description_string + '\'\n'

  ##### MTU
  elif suffix_string.find('mtu') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' link mtu ' + suffix_list[-1] + '\n'

  #### storage optimised/roce
  elif suffix_string.find('storage-optimized pfc') != -1:
    output_string = output_string + NVUE_SET + 'qos roce\n'

  ##### bridge pvid
  elif suffix_string.find('bridge pvid') != -1:
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' '
    output_string = output_string + suffix_string.replace('bridge pvid', 'bridge domain br_default untagged') + '\n'

  ##### bridge vids
  elif suffix_string.find('bridge vids') != -1:
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' '
    output_string = output_string + suffix_string.replace('bridge vids', 'bridge domain br_default vlan') + '\n'

  ##### bridge access
  elif suffix_string.find('bridge access') != -1:
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' '
    output_string = output_string + suffix_string.replace('bridge access', 'bridge domain br_default access') + '\n'

  ##### bridge allow-untagged
  elif suffix_string.find('bridge allow-untagged no') != -1:
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' '
    output_string = output_string + suffix_string.replace('bridge allow-untagged', 'bridge domain br_default untagged none') + '\n'

  ##### stp features
  elif suffix_string.find('stp') != -1:
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' '
    if   suffix_string.find('stp bpduguard') != -1:
      output_string = output_string + suffix_string.replace('stp bpduguard', 'bridge domain br_default stp bpdu-guard on') + '\n'
    elif suffix_string.find('stp portadminedge') != -1:
      output_string = output_string + suffix_string.replace('stp portadminedge', 'bridge domain br_default stp admin-edge on') + '\n'
    elif suffix_string.find('stp portautoedge no') != -1:
      output_string = output_string + suffix_string.replace('stp portautoedge', 'bridge domain br_default stp auto-edge off') + '\n'
    elif suffix_string.find('stp portbpdufilter') != -1:
      output_string = output_string + suffix_string.replace('stp portbpdufilter' ,'bridge domain br_default stp bpdu-filter on') + '\n'
    elif suffix_string.find('stp portnetwork') != -1:
      output_string = output_string + suffix_string.replace('stp portnetwork', 'bridge domain br_default stp network on') + '\n'
    elif suffix_string.find('stp portrestrole') != -1:
      output_string = output_string + suffix_string.replace('stp portrestrole', 'bridge domain br_default stp restrrole on') + '\n'
  
  ##### net add interface eth vrf mgmt
  elif int_type != 'vlan' and suffix_string.find('vrf') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip ' + suffix_string + '\n'

  elif suffix_string.find('pbr-policy') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router pbr map ' + suffix_list[-1]  + '\n'

  ##### ip address (vrr) # net add gateway 10.1.30.3/24
  elif suffix_string.find('ip gateway') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ' + suffix_string  + '\n'

  ##### ip address (vrr) # net add interface <> ip address 10.1.30.3/24
  elif (suffix_string.find('ip address') != -1 or suffix_string.find('ipv6 address') != -1) and (suffix_string.find('address-virtual') == -1):
    if suffix_string.find('ipv6 address') != -1:
      suffix_string = suffix_string.replace('ipv6','ip')
    ## Check for / (netmask) in the suffix string - Added in 1.9
    if suffix_string.find("/") == -1:
      suffix_string = suffix_string + "/32"
    # If eth0, unset the default DHCP configuration - CL 5.9.1
    if int_nvue_name == 'eth0':
      output_string = output_string  + NVUE_UNSET + 'interface eth0 ip address\n'
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ' + suffix_string  + '\n'

  ##### ip forward
  elif suffix_string.find('ip forward') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip ipv4 forward on' + '\n'

  ##### ip forward
  elif suffix_string.find('ip6-forward') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip ipv6 forward on' + '\n'

  ##### Neighbor Discovery
  ##### ipv6 nd suppress-ra
  elif suffix_string.find('ipv6 nd suppress-ra') != -1:
    if add_del == 'add':
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement enable off' + '\n'
    else:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement enable on'  + '\n'

  ##### ipv6 nd ra-interval
  elif suffix_string.find('ipv6 nd ra-interval') != -1:
    if suffix_list[3] == "msec":
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement interval ' + suffix_list[4] + '\n'
    else:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement interval ' + str(int(suffix_list[3]) * 1000) + '\n'

  ##### ipv6 nd adv-interval-option
  elif suffix_string.find('ipv6 nd adv-interval-option') != -1:
    if add_del == 'add':
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement interval-option on'  + '\n'
    else:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement interval-option off' + '\n'

  ##### ipv6 nd ra-fast-retrans
  elif suffix_string.find('ipv6 nd ra-fast-retrans') != -1:
    if add_del == 'add':
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router-advertisement fast-retransmit on'  + '\n'
    else:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router-advertisement fast-retransmit off' + '\n'

  ##### ipv6 nd ra-lifetime
  elif suffix_string.find('ipv6 nd ra-lifetime') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement lifetime ' + suffix_list[3] + '\n'

  ##### ipv6 nd reachable-time
  elif suffix_string.find('ipv6 nd reachable-time') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement reachable-time ' + suffix_list[3] + '\n'

  ##### ipv6 nd router-preference
  elif suffix_string.find('ipv6 nd router-preference') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery router-advertisement router-preference ' + suffix_list[3] + '\n'

  ##### ipv6 nd home-agent-lifetime
  elif suffix_string.find('ipv6 nd home-agent-lifetime') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery home-agent lifetime '  + suffix_list[3] + '\n'

  ##### ipv6 nd home-agent-preference
  elif suffix_string.find('ipv6 nd home-agent-preference') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery home-agent preference ' + suffix_list[3] + '\n'

  ##### ipv6 nd mtu
  elif suffix_string.find('ipv6 nd mtu') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery mtu ' + suffix_list[7] + '\n'

  ##### ipv6 nd prefix
  elif suffix_string.find('ipv6 nd prefix') != -1:
    base_str = NVUE_SET + 'interface ' + int_nvue_name + ' ip neighbor-discovery prefix ' + suffix_list[3]
    aux_args = []
    num_args = []
    future_support = False
    for n in range(8, len(suffix_list)):
      if suffix_list[n].isnumeric():
        num_args.append(suffix_list[n]) # valid-lifetime / preferred-lifetime
      elif suffix_list[n] == 'off-link':
        aux_args.append('off-link on')
      elif suffix_list[n] == 'no-autoconfig':
        aux_args.append('autoconfig off')
      else:  ## TODO?
        future_support = True
        break

    if not future_support:
      out_str_list = []
      handle_prefix_string(base_str + '\n', out_str_list)

      if len(num_args) >= 2:
        handle_prefix_string(base_str + ' valid-lifetime '    + num_args[0] + '\n', out_str_list)
        handle_prefix_string(base_str + ' preferred-lifetime ' + num_args[1] + '\n', out_str_list)

      for aux_arg in aux_args:
        handle_prefix_string(base_str + ' ' + aux_arg + '\n', out_str_list)

      output_string = output_string + ''.join(out_str_list)

  ### net add interface swp1 pbr-policy example
  elif (suffix_string.find('pbr-policy')!=-1):
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' router pbr map ' + suffix_list[2] + '\n'

  ### net add interface <> tunnel-
  elif (suffix_string.find('tunnel-endpoint') > 0):
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' tunnel dest-ip ' + suffix_list[2] + '\n'
  elif (suffix_string.find('tunnel-local') > 0):
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' tunnel source-ip ' + suffix_list[2] + '\n'
  elif (suffix_string.find('tunnel-mode') > 0):
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' tunnel mode ' + suffix_list[2] + '\n'
  elif (suffix_string.find('tunnel-ttl') > 0):
    output_string = NVUE_SET + 'interface ' + int_nvue_name + ' tunnel ttl ' + suffix_list[2] + '\n'
  
  ### EVPN MH link settings
  ##### EVPN MH Uplink:
  # net add interface swp51-54 evpn mh uplink -> nv set interface swp51-54 evpn multihoming uplink on
  elif suffix_string.find('evpn mh uplink') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' evpn multihoming uplink on'
  ##### EVPN DF preference for bonds only:
  # net add bond bond1-3 evpn mh es-df-pref 50000 -> nv set interface bond1-3 evpn multihoming segment df-preference 50000
  elif suffix_string.find('evpn mh es-df-pref') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' evpn multihoming segment df-preference ' + suffix_list[-1]

  ##### EVPN ESI MAC for bonds only:
  # net add bond bond1-3 evpn mh es-sys-mac 44:38:39:be:ef:aa -> nv set interface bond1 evpn multihoming segment mac-address <mac>
  elif suffix_string.find('evpn mh es-sys-mac') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' evpn multihoming segment mac-address ' + suffix_list[-1]

  ##### EVPN ESI ID for bonds only:
  # net add bond bond1 evpn mh es-id 1 -> nv set interface bond1 evpn multihoming segment local-id 1
  elif suffix_string.find('evpn mh es-id') != -1:
    output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' evpn multihoming segment local-id ' + suffix_list[-1]

  ### OSPF settings - Added in 1.7
  elif suffix_string.find(' ospf ') != -1 or suffix_string.find(' ospf6 ') != -1:
    if suffix_string.find('area') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf area ' + suffix_list[-1] + '\n'
    elif suffix_string.find('dead-interval') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf timers dead-interval ' + suffix_list[-1] + '\n'
    elif suffix_string.find('hello-interval') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf timers hello-interval ' + suffix_list[-1] + '\n'
    elif suffix_string.find('network') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf network-type ' + suffix_list[-1] + '\n'
    elif suffix_string.find('authentication') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf authentication enable on\n'
    elif suffix_string.find('bfd') != -1:
      if suffix_list[-1] == 'bfd':
        output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf bfd enable on\n'
      else:
        output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf bfd detect-multiplier ' + suffix_list[-3] + '\n'
        output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf bfd min-receive-interval ' + suffix_list[-2] + '\n'
        output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf bfd min-transmit-interval ' + suffix_list[-1] + '\n'
    elif suffix_string.find('cost') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf cost ' + suffix_list[-1] + '\n'
    elif suffix_string.find('message-digest-key') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf authentication message-digest-key ' + suffix_list[-3] + '\n'
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf authentication md5-key ' + suffix_list[-1] + '\n'
    elif suffix_string.find('mtu-ignore') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf mtu-ignore on\n'
    elif suffix_string.find('passive') != -1:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' router ospf passive on\n'
    else:
      output_string = output_string + f'# FUTURE SUPPORT - {line_str}\n'

  ### VLAN special cases
  if int_type=='vlan':
    # output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' type vlan\n'
    #### hwaddress
    if suffix_string.find('hwaddress')!=-1:
      # temp_list = line_str.split()
      #line_str = NVUE_SET + 'interface vlan' + temp_list[3] + ' ip vrr mac-address ' + temp_list[-1] + '\n' + \
      #NVUE_SET + 'interface vlan' + temp_list[3] + ' ip vrr state up '+'\n'
      output_string = output_string + f'# UNREQUIRED - {line_str}\n'

    ##### vrr address-virtual  # net add vlan 30 ip address-virtual 00:00:00:00:00:30 10.1.30.1/24
    elif (suffix_string.find('ip address-virtual')!=-1 or suffix_string.find('ipv6 address-virtual') != -1) and len(suffix_string.split())==4:
      output_string = output_string + NVUE_SET + 'interface '+ int_nvue_name +' ip vrr address ' + suffix_list[-1]+'\n' + \
      NVUE_SET + 'interface '+ int_nvue_name +' ip vrr mac-address ' + suffix_list[-2]+'\n' + \
      NVUE_SET + 'interface '+ int_nvue_name +' ip vrr state up '+'\n'

    ##### L3 VNI address-virtual # net add vlan 4001 ip address-virtual 44:38:39:BE:EF:AA
    elif suffix_string.find('ip address-virtual')!=-1 and len(suffix_string.split())==3:
      output_string = output_string + NVUE_SET + 'system global anycast-mac '+ suffix_list[-1]+'\n'

    ##### VLAN-id
    elif suffix_string.find('vlan-id')!=-1:
      if not int_name in L3_VNIs:
        output_string = output_string + NVUE_SET + 'interface '+ int_nvue_name +' vlan '+ int_name +'\n'

    #### vlan-raw-device
    elif suffix_string.find('vlan-raw-device')!=-1:
      output_string = output_string + '# UNREQUIRED - ' + line_str + '\n'

    #### VRF #net add vlan 4001 vrf RED
    elif suffix_string.find('vrf')!=-1:
      if not int_name in L3_VNIs:
        output_string = output_string + NVUE_SET + 'interface '+ int_nvue_name +' ip vrf ' + suffix_list[-1]+'\n'

  ## ACL interface - inbound|outbound
  if suffix_string.find('acl') > 0:
    if suffix_string.find('inbound') > 0:
      output_string = output_string + NVUE_SET + 'interface ' +  temp_list[-1] + ' acl ' + acl_id + 'inbound\n'
    elif suffix_string.find('outbound') > 0:
      output_string = output_string + NVUE_SET + 'interface ' +  temp_list[-1] + ' acl ' + acl_id + 'outbound\n'

  ## Port-security - Added in 1.7 and CL 5.7
  if suffix_string.find('port-security') > 0:
    if suffix_list[-1] == 'port-security':
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' port-security enable on\n'
    elif suffix_string.find('allowed-mac') > 0:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name + ' port-security static-mac ' + suffix_list[-1] + '\n'
    elif suffix_string.find('sticky-mac') > 0:
      if suffix_list[-1] == 'sticky-mac':
        output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name +' port-security sticky-mac enabled\n'
      elif suffix_string.find('timeout') > 0:
        output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name +' port-security sticky-timeout ' + suffix_list[-1] + '\n'
      elif suffix_string.find('aging') > 0:
        output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name +' port-security sticky-ageing enabled\n'
    elif suffix_string.find('mac-limit') > 0:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name +' port-security mac-limit ' + suffix_list[-1] + '\n'
    elif suffix_string.find('violation shutdown') > 0:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name +' port-security violation-mode protodown\n'
    elif suffix_string.find('violation timeout') > 0:
      output_string = output_string + NVUE_SET + 'interface ' + int_nvue_name +' port-security violation-timeout ' + suffix_list[-1] + '\n'

  return output_string


def main(argv):
  if not argv:    #check if no arg are entered
    print ('convert.py -i <inputfile> -o <outputfile> -c')
    print ('\n')
    print ('Usage:')
    print ('Input file must be specificed by using -i')
    print ('If -o is not specified then default of output.txt is used')
    print ('Optional -c will use cl set instead of the default of nv set')
    print ('Optional -d will debug the output')
    sys.exit()   # no arg, bugging out of here
  global inputfile,outputfile,NVUE,NVUE_SET,NVUE_UNSET,DEBUG  #use global variable
  try:
    opts, args = getopt.getopt(argv,'hi:o:c:d',['ifile=','ofile='])
  except getopt.GetoptError:
    print ('convert.py -i <inputfile> -o <outputfile> -c')
    print ('\n')
    print ('Usage:')
    print ('If -o is not specified then dafault of output.txt is used')
    print ('-c will use cl set, default is to use nv set')
    print ('-d will output debugged output line by line')
    sys.exit(2)
  for opt, arg in opts:
    if opt == '-h':
      print ('convert.py -i <inputfile> -o <outputfile> -c')
      print ('\n')
      print ('Usage:')
      print ('If -o is not specified then dafault of output.txt is used')
      print ('-c will use cl set, default is to use nv set')

      sys.exit()
    elif opt in ('-i', '--ifile'):
      inputfile = arg
    elif opt in ('-o', '--ofile'):
      outputfile = arg
    elif opt == '-c':
      NVUE = 'cl'

    elif opt == '-d':
      DEBUG = True
if __name__ == '__main__':
  main(sys.argv[1:])

line_output = []
debug_output = []
line_str = ''
temp_list = []
write_line = True
input_file = open(inputfile,'r')        # open the file for reading only
in_data = input_file.readlines()        # read the file into a list
output_file = open(outputfile,'w')        # open the file for writing to

if DEBUG:
  debug_file = open('debug.txt', 'w')





title = '#!/bin/bash\n# Converted from 4.x to 5.10\nset -x\n'   # add comment to file to say what created this file
output_file.write(title)

for line_data in in_data:
  line_str = line_data

  orig_str = line_str
  
  line_str = line_str.replace("\n", "")
  line_str = line_str.replace("\l", "")
  line_str = line_str.strip()
  if not line_str:
    continue

  if line_str.startswith('#'):
    continue

  # make sure everything is single spaced because script only works with single spacing
  line_str = line_str.replace('  ',' ')

  if line_str.startswith('net commit'):
    line_str= '# UNREQUIRED - ' + line_str
  elif not line_str.startswith('net'):
    line_str= '# SCRIPT UNSUPPORTED - ' + line_str

  ### Pre-filter all VRF to replace `-` with `_` - Not required with 5.3 and above
  # updated the code to also account for 'vrf' in the name of the vrf
  # if line_str.find(' vrf ') != -1:
  #   line_str = line_str.replace(line_str.split('vrf',1)[1].split()[0],line_str.split('vrf',1)[1].split()[0].replace('-','_'))

  ### Not implemented commands
  if line_str.find('pim')!=-1:
    line_str = line_str.replace('net add ', '# Not supported (PIM) - ')

  #### All interface features
  # Added vlan - Added in 1.3
  if line_str.startswith('net add interface') or line_str.startswith('net add bond') or line_str.startswith('net add vlan'):

    output_str = process_interfaces(line_str)
    if (output_str != ''):
      line_str = output_str
    else:
      line_str = '# FUTURE SUPPORT - ' + line_str

  ### NAT - Added support in 1.7 and CL 5.7
  elif line_str.startswith('net add nat '):
    nat_str = ''
    nat_acl = "acl_" + str(random.randint(0,1000))
    temp_list = line_str.split()
    if line_str.find('dynamic') != -1:
      nat_str = nat_str + NVUE_SET + 'system nat mode dynamic\n'
    nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' type ipv4\n'
    nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 match ip protocol ' + temp_list[5] + '\n'

    if line_str.find('snat') != -1:
      nat_type = "source"
    else:
      nat_type = "dest"
    
    if line_str.find('in-interface') != -1:
      nat_str = nat_str + NVUE_SET + 'interface ' + temp_list[temp_list.index('in-interface') + 1] + ' acl ' + nat_acl + ' inbound\n'
    if line_str.find('out-interface') != -1:
      nat_str = nat_str + NVUE_SET + 'interface ' + temp_list[temp_list.index('out-interface') + 1] + ' acl ' + nat_acl + ' outbound\n'
    
    if line_str.find('dynamic') != -1:
      if line_str.find('source-ip') != -1:
        nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 match ip source-ip ' + temp_list[temp_list.index('source-ip') + 1] + '\n'
      if line_str.find('dest-ip') != -1:
        nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 match ip dest-ip ' + temp_list[temp_list.index('dest-ip') + 1] + '\n'
      if line_str.find('source-port') != -1:
        nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 match ip ' + temp_list[5] + ' source-port ' + temp_list[temp_list.index('source-port') + 1] + '\n'
      if line_str.find('dest-port') != -1:
        nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 match ip ' + temp_list[5] + ' dest-port ' + temp_list[temp_list.index('dest-port') + 1] + '\n'
      if line_str.find('translate') != -1:
        trans_ip = temp_list[temp_list.index('translate') + 1]
        if trans_ip.find('-') != -1:
          trans_ip_split = trans_ip.split('-')
          trans_ip = trans_ip_split[0] + " to " + trans_ip_split[1]
        nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 action ' + nat_type + '-nat translate-ip ' + trans_ip + '\n'
        if temp_list[-3] == "translate":
          nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 action ' + nat_type + '-nat translate-port ' + temp_list[-1] + '\n'
    else:
      nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 match ip ' + nat_type + '-ip ' + temp_list[6] + '\n'
      if line_str.find('translate') != -1:
        nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 action ' + nat_type + '-nat translate-ip ' + temp_list[temp_list.index('translate') + 1] + '\n'
        if temp_list[-3] == "translate":
          nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 match ip ' + temp_list[5] + ' '+ nat_type + '-port ' + temp_list[7] + '\n'
          nat_str = nat_str + NVUE_SET + 'acl ' + nat_acl + ' rule 1 action ' + nat_type + '-nat translate-port ' + temp_list[-1] + '\n'

    

  ###SERVICES
  #### Hostname
  if line_str.startswith('net add hostname'):
    # line_str = line_str.replace('net add hostname', NVUE_SET + 'system hostname')
    # If hostname contains '.' like an FQDN, using only the first name - since NVUE does not support '.' as of CL 5.4
    temp_list = line_str.split()
    line_str = NVUE_SET + 'system hostname ' + temp_list[-1].split('.')[0]

  #### SNMP
  # SNMP support with 5.3 - Added in 1.3
  elif line_str.startswith('net add snmp-server'):
    temp_list = line_str.split()
    if line_str.find('system-') != -1: # account for system values containing a space
      line_str = NVUE_SET + 'service snmp-server ' + temp_list[3] + ' \'' + ' '.join(temp_list[4:]) + '\'\n'
    else:
      # account for special characters - updated in 1.5.1
      # catch net add snmp-server trap-snmp-auth-failures - updated in 1.6
      # catch net add snmp-server username <> auth-none - updated in 1.7
      if line_str.find('auth-') != -1 and line_str.find('auth-failures') == -1 and line_str.find('auth-none') == -1: 
        auth_mode = list(filter(lambda x: x.startswith('auth-'), temp_list))[0]
        line_str = line_str.replace(temp_list[temp_list.index(auth_mode) + 1],'\''+ temp_list[temp_list.index(auth_mode) + 1] + '\'')
        if line_str.find('encrypt-') != -1: # account for special characters - updated in 1.5.1
          encrypt_mode = list(filter(lambda x: x.startswith('encrypt-'), temp_list))[0]
          line_str = line_str.replace(temp_list[temp_list.index(encrypt_mode) + 1],'\''+ temp_list[temp_list.index(encrypt_mode) + 1] + '\'')
      if line_str.find('version') != -1 and temp_list[temp_list.index('version') + 1] == 'SSSS': # account for version SSSS - updated in 1.5.1
        line_str = line_str.replace('SSSS','2c')
      line_str = line_str.replace('net add snmp-server', NVUE_SET + 'service snmp-server')

  #### NTP - Updated in 1.3
  elif line_str.startswith('net add time ntp server'):
    # net add time ntp server 0.cumulusnetworks.pool.ntp.org iburst
    # nv set service ntp mgmt server 0.cumulusnetworks.pool.ntp.org iburst on
    vrf = check_vrf('ntp')
    line_str = line_str.replace('net add time ntp server', f'{NVUE_SET}service ntp {vrf} server')
    line_str = line_str.replace('iburst', f'iburst on')
  elif line_str.startswith('net add time ntp source'):
    vrf = check_vrf('ntp')
    line_str = line_str.replace('net add time ntp source', f'{NVUE_SET}service ntp {vrf} listen')
  elif line_str.startswith('net add time zone'):
    line_str = line_str.replace('net add time zone', f'{NVUE_SET}system timezone')

  #### PTP - Updated in 1.4
  elif line_str.startswith('net add ptp global'):
    if (line_str.find('priority1') != -1) or (line_str.find('priority2') != -1):
      temp_list = line_str.split()
      line_str = line_str.replace('net add ptp global',NVUE_SET + 'service ptp 1')
    elif (line_str.find('domain-number') != -1):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'service ptp 1 domain ' + temp_list[-1] + '\n'
    elif (line_str.find('time-stamping') != -1):
      line_str = NVUE_SET + 'service ptp 1 enable on\n'
    elif (line_str.find('slave-only') != -1) or (line_str.find('path-trace-enabled') != -1) or (line_str.find('use-syslog') != -1) or (line_str.find('verbose') != -1) or (line_str.find('summary-interval') != -1):
      line_str = '# UNREQUIRED - DEFAULT STATE NOT CONFIGURABLE BY NVUE - ' + orig_str
    elif (line_str.find('logging-level') != -1):
      line_str = ' SCRIPT UNSUPPORTED - ' + orig_str
    else:
      line_str = '# FUTURE SUPPORT - ' + orig_str

  #### Forwarding - Added in 1.3 and CL 5.2
  if line_str.startswith('net add forwarding ecmp'):
    line_str = line_str.replace('net add forwarding ecmp', NVUE_SET + 'system forwarding')

  #### DNS 
  elif line_str.startswith('net add dns nameserver'):
    # yes VRF
    if (line_str.find(' vrf ')!=-1):
      temp_list = line_str.split()
      vrf_name = temp_list[-1]
      line_str = line_str.replace(' vrf '+vrf_name,'')
    #no VRF
    else:
      vrf_name = 'default'
    temp_list = line_str.split()
    line_str = NVUE_SET +'service dns '+ vrf_name +' server ' +temp_list[-1] + '\n'

  #### Syslog
  elif line_str.startswith('net add syslog'):
    # yes VRF
    if (line_str.find(' vrf ')!=-1):
      temp_list = line_str.split()
      vrf_name = temp_list[-1]
      line_str = line_str.replace(' vrf '+vrf_name,'')
    #no VRF
    else:
      vrf_name = 'default'
    temp_list = line_str.split()
    line_str = NVUE_SET + 'service syslog ' + vrf_name + ' server ' + temp_list[-4] + ' port ' + temp_list[-1] + '\n' + \
      NVUE_SET + 'service syslog ' + vrf_name + ' server ' + temp_list[-4] + ' protocol ' + temp_list[-2] + '\n'

  #### DHCP Relay in default VRF - Updated in 1.3, 1.5 for CL 5.5
  elif line_str.startswith('net add dhcp relay server'):
    temp_list = line_str.split()
    line_str = NVUE_SET + 'service dhcp-relay default server ' + temp_list[-1] + '\n'
  elif line_str.startswith('net add dhcp relay interface'):
    temp_list = line_str.split()
    ## NVUE does not support ranges - Updated in 1.9
    if temp_list[-1].find('-')!=-1:
      line_str = "# MANUAL REVIEW " + orig_str + '\n'
    else:
      line_str = NVUE_SET + 'service dhcp-relay default interface ' + temp_list[-1] + '\n'
  elif line_str.startswith('net add dhcp relay use-giaddr-as-src'):
    line_str = NVUE_SET + 'service dhcp-relay source-ip giaddress' + '\n'
  elif line_str.startswith('net add dhcp relay giaddr-interface'):
    temp_list = line_str.split()
    if len(temp_list) == 7:
      giaddr = temp_list[-1]
    else:
      giaddr = 'auto'
    line_str = NVUE_SET + 'service dhcp-relay default gateway-interface ' + temp_list[-2] + ' address ' + giaddr + '\n'
  #### DHCP6 - Added in 1.3, updated in 1.5 for CL 5.5
  elif line_str.startswith('net add dhcp relay6'):
    temp_list = line_str.split()
    if len(temp_list) == 7:
      addr = temp_list[-1]
      line_str = line_str.replace(temp_list[-1],'').strip()
    else:
      addr = 'auto'
    # line_str = line_str.replace('net add dhcp relay6',NVUE_SET + 'service dhcp-relay6 default interface') + ' address ' + addr + '\n'
    if line_str.find('downstream') != -1:
      line_str = line_str.replace('net add dhcp relay6',NVUE_SET + 'service dhcp-relay6 default interface') + ' link-address ' + addr + '\n'
    else:
      line_str = line_str.replace('net add dhcp relay6',NVUE_SET + 'service dhcp-relay6 default interface') + ' server-address ' + addr + '\n'

  #### Dot1x - updated in 1.7
  elif line_str.startswith('net add dot1x'):
    line_str = '# NO SUPPORT (Broadcom based switches do not have an upgrade path to 5.x) - ' + orig_str

  #### QOS - Added in Cl 5.3, updated in 1.4
  elif line_str.startswith('net add qos egress scheduler'):
    temp_list = line_str.split()
    if line_str.find('queue') != -1:
      line_str = NVUE_SET + 'qos egress-scheduler ' + temp_list[temp_list.index('profile') + 1] + ' traffic-class ' + temp_list[temp_list.index('queue') + 1] + ' mode ' +  temp_list[temp_list.index('queue') + 2] + '\n'
      if line_str.find('bw-percent') != -1:
        line_str = NVUE_SET + 'qos egress-scheduler ' + temp_list[temp_list.index('profile') + 1] + ' traffic-class ' + temp_list[temp_list.index('queue') + 1] + ' bw-percent ' +  temp_list[-1] + '\n'
    else:
      line_str = NVUE_SET + 'qos egress-scheduler ' + temp_list[temp_list.index('profile') + 1] + '\n'

  ### Platform and User Accounts
  #### User management
  elif line_str.startswith('net add username'):
    # Since cumulus user account is available with default, add below section - Updated in 1.7
    if line_str.find(' cumulus ') != -1:
      line_str = '# UNREQUIRED - ' + orig_str
    line_str = '# FUTURE SUPPORT - ' + orig_str

  ###Routing Defaults
  elif line_str.startswith('net add routing defaults datacenter'):
    line_str = '# UNREQUIRED - ' + orig_str
  elif line_str.startswith('net add routing log syslog'):
    line_str = '# UNREQUIRED - ' + orig_str
  elif line_str.startswith('net add routing service integrated-vtysh-config'):
    line_str = '# UNREQUIRED - ' + orig_str

  ### Static Routes - fixed bugs in 1.3
  #### no VRF
  elif line_str.startswith('net add routing route ') and (line_str.find(' vrf ')==-1):
    temp_list = line_str.split()
    line_str = NVUE_SET +'vrf default router static ' + temp_list[temp_list.index('route') + 1] + ' via ' + temp_list[temp_list.index('route') + 2]
    if 'onlink' in temp_list:
      line_str = line_str + ' flag onlink'
    line_str = line_str + '\n'
    if 'nexthop-vrf' in temp_list:
      # line_str = '# WARNING - check the BGP requirements for dynamic leaking\n' + '# MANUAL REVIEW - ' + orig_str + '\n'
      line_str = NVUE_SET + 'vrf default router bgp address-family ipv4-unicast route-import from-vrf list ' + temp_list[temp_list.index('nexthop-vrf') + 1] + '\n'
      line_str = line_str + NVUE_SET + 'vrf default router bgp address-family ipv4-unicast redistribute connected enable on\n'
    
  #### yes VRF
  elif line_str.startswith('net add routing route ') and (line_str.find(' vrf ') != -1):
    temp_list = line_str.split()
    line_str = NVUE_SET +'vrf ' + temp_list[temp_list.index('vrf') + 1] +' router static ' + temp_list[temp_list.index('route') + 1] + ' via ' + temp_list[temp_list.index('route') + 2]
    if 'onlink' in temp_list:
      line_str = line_str + ' flag onlink'
    line_str = line_str + '\n'
    if 'nexthop-vrf' in temp_list:
      # line_str = '# WARNING - check the BGP requirements for dynamic leaking\n' + '# MANUAL REVIEW - ' + orig_str + '\n'
      line_str = NVUE_SET + 'vrf ' + temp_list[temp_list.index('vrf') + 1] + ' router bgp address-family ipv4-unicast route-import from-vrf list ' + temp_list[temp_list.index('nexthop-vrf') + 1] + '\n'
      line_str = line_str + NVUE_SET + 'vrf ' + temp_list[temp_list.index('vrf') + 1] + ' router bgp address-family ipv4-unicast redistribute connected enable on\n'

  ### Routing as-path - Added in 1.7
  elif line_str.startswith('net add routing as-path access-list'):
    temp_list = line_str.split()
    line_str = NVUE_SET + 'router policy as-path-list ' + temp_list[5] + ' rule 1 aspath-exp ' + temp_list[-1] + '\n'
    line_str = line_str + NVUE_SET + 'router policy as-path-list ' + temp_list[5] + ' rule 1 action ' + temp_list[6] + '\n'

  ### Route-maps - updated with 1.5.1
  elif line_str.startswith('net add routing route-map'):
    temp_list = line_str.split()
    permit_deny = temp_list[5]
    router_policy = 0
    if line_str.find('description') > 0:
      description_string = ' '.join(temp_list[temp_list.index('description') + 1:]).replace('-','_').replace(':','_') 
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' description \'' + description_string + '\'\n' 
      router_policy = 1  
    # added in version 1.7
    # net add routing route-map RM-LBHOST-IN deny 65535
    elif temp_list[-2] == 'permit' or temp_list[-2] == 'deny':
      router_policy = 1  
    elif line_str.find('match community') > 0: 
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match community-list ' + temp_list[temp_list.index('community') + 1] + '\n'
      router_policy = 1 
    # Added in version 1.7
    elif line_str.find('match large-community') > 0: 
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match large-community-list ' + temp_list[temp_list.index('large-community') + 1] + '\n'
      router_policy = 1 
    # Added in version 1.5.1
    elif line_str.find('match interface') > 0 or line_str.find('match local-preference') > 0 or line_str.find('match metric') > 0 or line_str.find('match origin') > 0 or line_str.find('match peer') > 0 or line_str.find('match source-protocol') > 0 or line_str.find('match source-vrf') > 0 or line_str.find('match tag') > 0: 
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match ' + temp_list[temp_list.index('match') + 1] + ' ' + temp_list[-1] + '\n'
      router_policy = 1 
    # Added in version 1.5.1
    elif line_str.find('match ip') > 0: 
      router_policy = 1
      if line_str.find('address prefix-len') > 0: 
        line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match ip-prefix-len ' + temp_list[temp_list.index('prefix-len') + 1] + '\n'
      elif line_str.find('next-hop prefix-len') > 0: 
        line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match ip-nexthop-len ' + temp_list[temp_list.index('prefix-len') + 1] + '\n'
      elif line_str.find('address prefix-list') > 0: 
        line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match ip-prefix-list ' + temp_list[temp_list.index('prefix-list') + 1] + '\n'
      elif line_str.find('next-hop prefix-len') > 0: 
        line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match ip-nexthop-list ' + temp_list[temp_list.index('prefix-list') + 1] + '\n'
      else:
        router_policy = 0
    # Added in version 1.7
    elif line_str.find('match as-path') > 0: 
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match as-path-list ' + temp_list[temp_list.index('as-path') + 1] + '\n'
      router_policy = 1
    elif line_str.find('evpn default-route') > 0:
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match evpn-default-route on\n' # CL Version added - 5.2
      router_policy = 1
    elif line_str.find('evpn route-type') > 0:
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match evpn-route-type ' +  temp_list[-1] + '\n'
      router_policy = 1
    elif line_str.find('evpn vni') > 0:
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' match evpn-vni ' +  temp_list[-1] + '\n'
      router_policy = 1
    elif line_str.find('prefer-global') > 0: #next-hop prefer-global
      #nv set router policy route-map example rule 1 set ipv6-nexthop-prefer-global on
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' set ipv6-nexthop-prefer-global on\n'
      router_policy = 1
    elif line_str.find('set as-path prepend last-as') > 0:
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' set as-path-prepend last-as ' + temp_list[-1] + '\n' # CL Version added - 5.2
      router_policy = 1
    elif line_str.find('set as-path prepend') > 0:
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' set as-path-prepend as ' + ' '.join(temp_list[10:]) + '\n' # CL Version added - 5.2
      router_policy = 1
    elif line_str.find('set originator-id') > 0 or line_str.find('set label-index') > 0 or line_str.find('set forwarding-address') > 0: # CL Version added - 5.2
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' set ' + temp_list[temp_list.index('set') + 1] + ' ' + temp_list[-1] + '\n' 
      router_policy = 1
    # Added in version 1.5.1
    elif line_str.find('set atomic-aggregate') > 0 or line_str.find('set local-preference') > 0 or line_str.find('set metric') > 0 or line_str.find('set metric-type') > 0 or line_str.find('set origin') > 0 or line_str.find('set tag') > 0 or line_str.find('set weight') > 0:
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' set ' + temp_list[temp_list.index('set') + 1] + ' ' + temp_list[-1] + '\n' 
      router_policy = 1
    else:
      line_str = '# FUTURE SUPPORT - ' + line_str
    
    # If router route-map policy was mapped, add the deny/permit action
    if router_policy:
      line_str = line_str + NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' action ' + permit_deny + '\n'

  elif line_str.startswith('net del routing route-map'):
    temp_list = line_str.split()
    if line_str.find('prefer-global') > 0: #next-hop prefer-global
      line_str = NVUE_SET + 'router policy route-map ' + temp_list[4] + ' rule ' + temp_list[6] + ' set ipv6-nexthop-prefer-global ' + 'off' + '\n'

  ### Prefix-lists - Added in 1.7
  elif line_str.startswith('net add routing prefix-list'):
    temp_list = line_str.split()
    prefix_list_type = temp_list[4]
    router_pl_policy = 0 
    if line_str.find('deny') > 0 or line_str.find('permit') > 0:
      if line_str.find('seq') != -1:
        rule_id = temp_list[temp_list.index('seq') + 1] 
        match_id = temp_list[9]
        permit_deny = temp_list[8]
      else:
        rule_id = 1
        match_id = temp_list[7]
        permit_deny = temp_list[6]
      line_str = NVUE_SET + 'router policy prefix-list ' + temp_list[5] + ' rule ' + rule_id + ' match ' + match_id + '\n'
      router_pl_policy = 1
      if orig_str.find(' ge ') > 0: 
        line_str = line_str + NVUE_SET + 'router policy prefix-list ' + temp_list[5] + ' rule ' + rule_id + ' match ' + match_id + ' min-prefix-len ' + temp_list[temp_list.index('ge') + 1] + '\n'
      if orig_str.find(' le ') > 0: 
        line_str = line_str + NVUE_SET + 'router policy prefix-list ' + temp_list[5] + ' rule ' + rule_id + ' match ' + match_id + ' max-prefix-len ' + temp_list[temp_list.index('le') + 1] + '\n'
    else:
      line_str = '# FUTURE SUPPORT - ' + line_str
    
    # If router prefix-list policy was mapped, add the deny/permit action
    if router_pl_policy:
      line_str = line_str + NVUE_SET + 'router policy prefix-list ' + temp_list[5] + ' rule ' + rule_id + ' action ' + permit_deny + '\n'
      line_str = line_str + NVUE_SET + 'router policy prefix-list ' + temp_list[5] + ' type ' + prefix_list_type + '\n'
  
  ### PBR maps
  #### nexthop-group
  elif line_str.startswith('net add pbr-map'):
    temp_list = line_str.split()
    if line_str.find('nexthop-group') > 0:
      line_str = NVUE_SET + 'router pbr map ' + temp_list[3] + ' rule ' + temp_list[5] + ' set nexthop-group ' + temp_list[8] + '\n'

  ### BGP - Code updated in 1.7
  ### Identify if VRF exists in the definition and set it to vrf_name variable, else set vrf_name variable to default
  elif line_str.startswith('net add bgp'):
    if line_str.startswith('net add bgp vrf '):
      # not using line_str.find(' vrf ') to avoid catching statements with vrf in the later part of the line - eg: net add bgp vrf VRF_AUH_DMZ ipv4 unicast import vrf route-map SHARED-SERVICES-IMPORT
      temp_list = line_str.split()
      vrf_name = temp_list[4]
      line_str = line_str.replace(' vrf ' + vrf_name,'') 
    else:
      vrf_name = 'default'
  
    #### Autonomous System
    if line_str.startswith('net add bgp autonomous-system'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp autonomous-system ' + temp_list[-1] + '\n'

    #### Router-ID
    elif line_str.startswith('net add bgp router-id'):
      temp_list = line_str.split()
      line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp router-id ' + temp_list[-1] +'\n'

    #### Bestpath as-path multipath relax
    elif line_str.startswith('net add bgp bestpath as-path multipath-relax'):
      line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp path-selection multipath aspath-ignore on\n'

    #### Med compare always - Added in 1.3
    elif line_str.startswith('net add bgp always-compare-med'):
      line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp path-selection med compare-always on\n'

    #### Bestpath compare router-id - Added in 1.3
    elif line_str.startswith('net add bgp bestpath compare-routerid'):
      line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp path-selection routerid-compare on\n'

    #### Cluster-ID - Added in 1.3
    elif line_str.startswith('net add bgp cluster-id'):
      line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp route-reflection cluster-id ' + temp_list[-1] + '\n'

    #### Graceful shutdown - Added in 1.3
    elif line_str.startswith('net add bgp graceful-shutdown'):
      line_str = NVUE_SET + 'router bgp graceful-shutdown on\n'

    #### Listen Range - Added in 1.7 and CL 5.2
    elif line_str.startswith('net add bgp listen range'):
      line_str = line_str.replace('net add bgp listen range', NVUE_SET + 'vrf ' + vrf_name + ' router bgp dynamic-neighbor listen-range')
    
    #### Network - Added in 1.7
    elif line_str.startswith('net add bgp network'):
      temp_list = line_str.split()
      ip_type = validIPAddress(temp_list[4])
      if ip_type == 'IPv4':
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family ipv4-unicast network ' + temp_list[4]
        if orig_str.find('route-map') != -1:
          line_str = line_str + ' route-map ' + temp_list[temp_list.index('route-map') + 1]
        line_str = line_str + '\n'
      elif ip_type == 'IPv6':
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family ipv6-unicast network ' + temp_list[4]
        if orig_str.find('route-map') != -1:
          line_str = line_str + ' route-map ' + temp_list[temp_list.index('route-map') + 1]
        line_str = line_str + '\n'
      else:
        line_str = '# UNSUPPORTED -' + line_str 

    #### Neighbor
    elif line_str.startswith('net add bgp neighbor'):
      ##### Peer-group
      if (line_str.find('peer-group')!=-1) and (line_str.find('interface')==-1):
        temp_list = line_str.split()
        if (temp_list[-1] == 'peer-group'):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[-2]+'\n'
        elif (temp_list[-2] == 'peer-group'):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp neighbor ' + temp_list[-3] + ' peer-group ' + temp_list[-1] + '\n'
      
      ##### Peer-group membership interface
      elif (line_str.find('interface peer-group')!=-1):
        temp_list = line_str.split()
        line_str = NVUE_SET +'vrf ' + vrf_name + ' router bgp neighbor ' + temp_list[4] + ' type unnumbered\n' + \
        NVUE_SET + 'vrf ' + vrf_name + ' router bgp neighbor ' + temp_list[4] + ' peer-group ' + temp_list[-1]+'\n'

      ##### Peer-group remote-as
      elif (line_str.find('remote-as')!=-1) and (line_str.find('interface')==-1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[-3] + ' remote-as ' + temp_list[-1] + '\n'

      ##### Peer-group remote-as interface
      elif (line_str.find('interface remote-as')!=-1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp neighbor ' + temp_list[4] + ' type unnumbered\n' + \
        NVUE_SET + 'vrf ' + vrf_name + ' router bgp neighbor ' + temp_list[4] + ' remote-as ' + temp_list[-1]+'\n'

      ##### Allowas - Added in 1.9
      elif (line_str.find('allowas-in')!=-1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[-3] + ' address-family ipv4-unicast aspath allow-my-asn enable on\n'
        if (line_str.find('allowas-in origin')!=-1):
          line_str = line_str + NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[-3] + ' address-family ipv4-unicast aspath allow-my-asn origin on\n'
        else:
          line_str = line_str + NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[-3] + ' address-family ipv4-unicast aspath allow-my-asn occurrences ' + temp_list[-1] +'\n'

      ##### BFD - Updated in 1.6
      elif (line_str.find('bfd')!=-1):
        temp_list = line_str.split()
        mult  = temp_list[-3] if temp_list[-3].isnumeric() else '3'
        rxint = temp_list[-2] if temp_list[-2].isnumeric() else '300'
        txint = temp_list[-1] if temp_list[-1].isnumeric() else '300'
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' bfd enable on\n' + \
        NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' bfd detect-multiplier ' + mult + '\n' + \
        NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' bfd min-rx-interval ' + rxint + '\n' + \
        NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' bfd min-tx-interval ' + txint +'\n'

      ##### Timers - Updated in 1.7
      elif (line_str.find('timers')!=-1):
        temp_list = line_str.split()
        if (line_str.find('connect') == -1):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' timers keepalive ' + temp_list[-2] + '\n'
          line_str = line_str + NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' timers hold ' + temp_list[-1] + '\n'
        else:
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' timers connection-retry ' + temp_list[-1] + '\n'

      ##### Extended-nexthop
      elif (line_str.find('capability extended-nexthop')!=-1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' capabilities extended-nexthop on' + '\n'

      ##### Additional settings - Updated in 1.3.1
      elif (line_str.find('graceful-restart-mode helper-and-restarter') > 0):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' graceful-restart mode full' + '\n'
      elif (line_str.find('graceful-restart-mode helper') > 0):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' graceful-restart mode helper-only' + '\n'
      elif (line_str.find('description') > 0):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' description \'' + ' '.join(temp_list[6:]) + '\'\n' #accounting for multiple strings in description
      elif(line_str.find('update-source') > 0):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' update-source ' + temp_list[-1] + '\n'
      elif(line_str.find('ebgp-multihop') > 0):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' multihop-ttl ' + temp_list[-1] + '\n'
      elif(line_str.find('ttl-security hops') > 0):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' ttl-security hops ' + temp_list[-1] + '\n'
      elif(line_str.find('shutdown') > 0):
        temp_list = line_str.split()
        line_str = line_str + NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[4] + ' shutdown on\n'

    #### Unicast address family - Updated in 1.3 (Made code common for ipv4 and ipv6 + added additional settings)
    elif line_str.startswith('net add bgp ipv4 unicast') or line_str.startswith('net add bgp ipv6 unicast') or line_str.startswith('net add bgp ip unicast'):
      # Identify the address family and replace it in original string with 'ip'
      if line_str.startswith('net add bgp ipv4 unicast'):
        addr_family = ' ipv4-unicast '
        line_str = line_str.replace('ipv4 unicast','ip unicast')
      elif line_str.startswith('net add bgp ipv6 unicast'):
        addr_family = ' ipv6-unicast '
        line_str = line_str.replace('ipv6 unicast','ip unicast')
      elif line_str.startswith('net add bgp ip unicast'):
        addr_family = ' ipv4-unicast '

      ##### network
      if line_str.startswith('net add bgp ip unicast network'):
        line_str = line_str.replace('net add bgp ip unicast network', NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family' + addr_family + 'network')

      ##### redistribute - Updated in 1.3
      elif line_str.startswith('net add bgp ip unicast redistribute'):
        if (line_str.find('table') == -1): # net add bgp (ipv4|ipv6) unicast redistribute table <> not supported with NVUE
          temp_list = line_str.split()
          if line_str.find('metric') > 0:
            line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family' + addr_family + 'redistribute ' + temp_list[6] + ' metric ' + temp_list[8] + '\n'
          if line_str.find('route-map') > 0:
            line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family' + addr_family + 'redistribute ' + temp_list[6] + ' route-map ' + temp_list[-1] + '\n'
          else:
            line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family' + addr_family + 'redistribute ' + temp_list[6] +'\n'

      ##### next-hop-self
      elif line_str.startswith('net add bgp ip unicast neighbor') and (line_str.find('next-hop-self')!=-1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp neighbor ' + temp_list[-2] + ' address-family' + addr_family + 'nexthop-setting self\n'

      #### allowas-in
      elif line_str.startswith('net add bgp ip unicast neighbor') and (line_str.find('allowas-in')!=-1):
        temp_list = line_str.split()
        if (line_str.find('allowas-in origin')!=-1):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[-3] + ' address-family' + addr_family + 'aspath allow-my-asn origin on\n'
        else:
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[-3] + ' address-family' + addr_family + 'aspath allow-my-asn occurrences ' + temp_list[-1] +'\n'
      
      ##### advertise ipv4 unicast
      elif (line_str.find('ip unicast import vrf route-map') > 0):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family' + addr_family + 'route-import from-vrf route-map ' + temp_list[-1] + '\n'
      elif (line_str.find('ip unicast import vrf') > 0):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family' + addr_family + 'route-import from-vrf list ' + temp_list[-1] + '\n'

      ##### aggregate-address - Added in 1.3
      elif (line_str.find('ip unicast aggregate-address') > 0):
        temp_list = line_str.split()
        if (line_str.find('as-set') > 0) or (line_str.find('summary-only') > 0):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family' + addr_family + 'aggregate-route ' + temp_list[6] + ' ' + temp_list[-1] + ' on\n'

      #### Peer-Group IPv4 unicast settings - Added in 1.3
      elif (line_str.find('neighbor') != -1):
        temp_list = line_str.split()
        ##### activate
        if (line_str.find('activate') != -1):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'enable on\n'

        ##### addpath-tx
        elif (line_str.find('addpath-tx-all-paths') != -1):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'add-path-tx all-paths\n'
        elif (line_str.find('addpath-tx-bestpath-per-AS') != -1):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'add-path-tx best-per-as\n'
          
        ##### route-map
        elif (line_str.find('route-map') != -1):
          if(temp_list[7] == 'route-map'):
            # net add bgp ipv4 unicast neighbor <bgppeer> route-map <route-map> (in|out)
            line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'policy ' + temp_list[-1] + 'bound route-map ' + temp_list[-2] + '\n'
          elif (line_str.find('default-originate route-map') != -1):
            # net add bgp ipv4 unicast neighbor <bgppeer> default-originate route-map <route-map>
            line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'default-route-origination policy ' + temp_list[-1] + '\n'

        ##### route-reflector-client
        elif (line_str.find('route-reflector-client') != -1):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'route-reflector-client on\n'
        
        ##### send-community - Added in 1.3.1
        elif (line_str.find('neighbor') != -1) and (line_str.find('send-community') != -1):
          temp_list = line_str.split()
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'community-advertise '
          if(line_str.find('both')):
            line_str = line_str + 'regular on\n' + line_str + 'extended on\n'
          elif(line_str.find('extended')):
            line_str = line_str + 'extended on\n'
          elif(line_str.find('standard')):
            line_str = line_str + 'regular on\n'

        ##### soft-reconfiguration - Added in 1.3.1
        elif (line_str.find('neighbor') != -1) and (line_str.find('soft-reconfiguration inbound') != -1):
          temp_list = line_str.split()
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'soft-reconfiguration on'

        ##### prefix-list
        elif (line_str.find('prefix-list') != -1):
          line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'policy ' + temp_list[-1] + 'bound prefix-list ' + temp_list[-2] + '\n'

        ##### Remove Private BGP ASNs
        elif (line_str.find('remove-private-AS') != -1):
          if (line_str.find('replace-AS') != -1):
            line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'aspath private-as remove\n'
          else:
            line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family' + addr_family + 'aspath replace-peer-as on\n'

    #### L2VPN EVPN address family
    if line_str.startswith('net add bgp l2vpn evpn'):
      ##### Activate
      if line_str.startswith('net add bgp l2vpn evpn neighbor peerlink.4094 activate'):
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp neighbor peerlink.4094 address-family l2vpn-evpn enable on\n'
      elif line_str.startswith('net add bgp l2vpn evpn neighbor swp') and (line_str.find('activate')!=-1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp neighbor ' + temp_list[temp_list.index('neighbor') + 1] + ' address-family l2vpn-evpn enable on\n'
      elif line_str.startswith('net add bgp l2vpn evpn neighbor') and (line_str.find('activate')!=-1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[-2] + ' address-family l2vpn-evpn enable on\n' + \
        NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family l2vpn-evpn enable on\n'

      ##### route-map - Added in 1.3 - future support till we add support for route-maps
      elif (line_str.find('neighbor') != -1) and (line_str.find('route-map') != -1):
        # temp_list = line_str.split()
        # line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family l2vpn-evpn policy ' + temp_list[-1] + 'bound route-map ' + temp_list[-2] + '\n'
        line_str = "# FUTURE SUPPORT - " + line_str

      ##### route-reflector-client - Added in 1.3
      elif (line_str.find('neighbor') != -1) and (line_str.find('route-reflector-client') != -1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp peer-group ' + temp_list[6] + ' address-family l2vpn-evpn route-reflector-client on\n'

      ##### advertise ipv4 unicast
      elif (line_str.find('l2vpn evpn advertise ipv4 unicast') != -1):
        temp_list = line_str.split()
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router bgp address-family ipv4-unicast route-export to-evpn\n'

      ##### advertise-all-vni
      elif line_str.startswith('net add bgp l2vpn evpn advertise-all-vni'):
        line_str = NVUE_SET + 'evpn enable on\n'

      ##### advertise-default-gw - Added in 1.3
      elif line_str.startswith('net add bgp l2vpn evpn advertise-default-gw'):
        line_str = NVUE_SET + 'evpn route-advertise default-gateway on\n'

      ##### advertise-svi-ip generic
      elif line_str.startswith('net add bgp l2vpn evpn advertise-svi-ip'):
        temp_list = line_str.split()
        line_str = NVUE_SET +'evpn route-advertise svi-ip on\n'

      ##### VNI
      elif line_str.startswith('net add bgp l2vpn evpn vni'):
        ##### advertise-svi-ip VNI 
        if line_str.find('advertise-svi-ip')!=-1:
          temp_list = line_str.split()
          # line_str = NVUE_SET +'evpn evi ' + temp_list[6] +' route-advertise svi-ip on\n'
          # nv set evpn evi changed to nv set evpn vni in CL 5.4
          line_str = NVUE_SET +'evpn vni ' + temp_list[6] +' route-advertise svi-ip on\n'
        ##### advertise-default-gw VNI- Added in 1.3
        elif line_str.find('advertise-default-gw')!=-1:
          temp_list = line_str.split()
          # line_str = NVUE_SET +'evpn evi ' + temp_list[6] +' route-advertise default-gateway on\n'
          line_str = NVUE_SET +'evpn vni ' + temp_list[6] +' route-advertise default-gateway on\n'
        ##### route-target import/export and rd - Added in 1.3
        elif line_str.find('route-target')!=-1 or line_str.find('rd')!=-1 :
          # line_str = line_str.replace('net add bgp l2vpn evpn vni', NVUE_SET + 'evpn evi') + '\n'
          line_str = line_str.replace('net add bgp l2vpn evpn vni', NVUE_SET + 'evpn vni') + '\n'

      ##### dup-addr-detection - Added in 1.3
      elif line_str.startswith('net add bgp l2vpn evpn dup-addr-detection'):
        temp_list = line_str.split()
        if line_str.find('freeze')!=-1:
          line_str = NVUE_SET +'evpn dad duplication-action freeze duration ' + temp_list[-1] + '\n'
        elif line_str.find('max-moves')!=-1:
          line_str = NVUE_SET +'evpn dad mac-move-threshold ' + temp_list[-3] + '\n' \
                    + NVUE_SET +'evpn dad move-window ' + temp_list[-1] + '\n'

  #### VRF and VNI mapping
  elif line_str.startswith('net add vrf') and (line_str.find('vni') != -1):
    temp_list = line_str.split()
    line_str = NVUE_SET +'vrf ' + temp_list[3] + ' evpn vni ' + temp_list[-1] + '\n'
    L3_VNIs.add(temp_list[-1])

  
  ### Interface

  ### EVPN MH link settings
  ##### EVPN MH Uplink:
  # net add interface swp51-54 evpn mh uplink -> nv set interface swp51-54 evpn multihoming uplink on
  elif line_str.startswith('net add interface') and (line_str.find('evpn mh uplink')!=-1):
    line_str = line_str.replace('net add ',NVUE_SET)
    line_str = line_str.replace('mh uplink','multihoming uplink on')

  ##### EVPN DF preference for bonds only:
  # net add bond bond1-3 evpn mh es-df-pref 50000 -> nv set interface bond1-3 evpn multihoming segment df-preference 50000
  elif line_str.startswith('net add bond') and (line_str.find('evpn mh es-df-pref')!=-1):
    line_str = line_str.replace('net add ',NVUE_SET)
    line_str = line_str.replace(' bond ',' interface ')
    line_str = line_str.replace('mh es-df-pref','multihoming segment df-preference')

  ##### EVPN ESI MAC for bonds only:
  # net add bond bond1-3 evpn mh es-sys-mac 44:38:39:be:ef:aa -> nv set interface bond1 evpn multihoming segment mac-address <mac>
  elif line_str.startswith('net add') and (line_str.find('evpn mh es-sys-mac')!=-1):
    line_str = line_str.replace('net add ',NVUE_SET)
    line_str = line_str.replace(' bond ',' interface ')
    line_str = line_str.replace('mh es-sys-mac','multihoming segment mac-address')

  ##### EVPN ESI ID for bonds only:
  # net add bond bond1 evpn mh es-id 1 -> nv set interface bond1 evpn multihoming segment local-id 1
  elif line_str.startswith('net add') and (line_str.find('evpn mh es-id')!=-1):
    line_str = line_str.replace('net add ',NVUE_SET)
    line_str = line_str.replace(' bond ',' interface ')
    line_str = line_str.replace('mh es-id','multihoming segment local-id')

  ##### EVPN MH startup delay:
  # net add evpn mh startup-delay 10 -> nv set evpn  multihoming startup-delay
  elif line_str.startswith('net add evpn mh startup-delay'):
    line_str = line_str.replace('net add ',NVUE_SET)
    line_str = line_str.replace('mh startup-delay','multihoming startup-delay')
  #### Bridge

  #### VXLAN VNI
  elif line_str.startswith('net add vxlan') and (line_str.find('arp-nd-suppress')!=-1):
    line_str = 'nv set nve vxlan arp-nd-suppress on\n'
  elif line_str.startswith('net add vxlan') and (line_str.find('vxlan id')!=-1):
    # net add vxlan vni10 vxlan id 10
    temp_list = line_str.split()
    vni_intf = VNI_STRUCT.get(temp_list[3], dict())
    vni_intf['vni'] = temp_list[6]
    VNI_STRUCT[temp_list[3]] = vni_intf
    line_str = f'# Command handled via VNI_STRUCT (see bottom of the file) -- {line_str}'
  elif line_str.startswith('net add vxlan') and (line_str.find('bridge access')!=-1):
    # net add vxlan vni10 bridge access 10
    temp_list = line_str.split()
    vni_intf = VNI_STRUCT.get(temp_list[3], dict())
    vni_intf['vlan'] = temp_list[6]
    VNI_STRUCT[temp_list[3]] = vni_intf
    line_str = f'# Command handled via VNI_STRUCT (see bottom of the file) -- {line_str}'
  elif line_str.startswith('net add vxlan'):
    line_str = '\n'
  elif line_str.startswith('net add interface') and (line_str.find('bridge vids')!=-1):
    line_str = line_str.replace('bridge vids','bridge domain br_default vlan')

  #### vxlan-anycast-ip
  elif line_str.startswith('net add loopback lo clag vxlan-anycast-ip'):
    temp_list = line_str.split()
    line_str = NVUE_SET + 'nve vxlan mlag shared-address ' + temp_list[-1] + '\n'

  #### local tunnelip # net add loopback lo vxlan local-tunnelip 10.10.10.2
  elif line_str.startswith('net add loopback lo vxlan local-tunnelip'):
    temp_list = line_str.split()
    line_str = NVUE_SET + 'nve vxlan source address ' + temp_list[-1] + '\n'

  #### Loopback
  elif line_str.startswith('net add loopback lo'):
    if (line_str.find('alias')!=-1):
      line_str = '# FUTURE SUPPORT - ' + orig_str
    elif (line_str.find('lo ipv6 address')!=-1):
      line_str = line_str.replace('net add loopback lo ipv6',NVUE_SET + 'interface lo ip')
    else:
      line_str = line_str.replace('net add loopback',NVUE_SET + 'interface')

  ### VRF
  #### VRF definition
  elif line_str.startswith('net add vrf') and (line_str.find('vrf-table')!=-1):
    temp_list = line_str.split()
    vrf_list = temp_list[3].split(',')
    for vrf in vrf_list:
      line_str = NVUE_SET + 'vrf '+ vrf +'\n'
      line_output.append(line_str)
      write_line = False

  #### VRF loopback
  elif line_str.startswith('net add vrf') and (line_str.find('ip')!=-1) and (line_str.find('address')!=-1):
    temp_list = line_str.split()
    vrf_list = temp_list[3].split(',')
    for vrf in vrf_list:
      line_str = NVUE_SET + 'vrf '+ vrf + ' loopback ip address ' + temp_list[-1] + '\n'
      line_output.append(line_str)
      write_line = False

  ##### EVPN MH startup delay:
  # net add evpn mh startup-delay 10 -> nv set evpn  multihoming startup-delay
  elif line_str.startswith('net add evpn mh startup-delay'):
    line_str = line_str.replace('net add ',NVUE_SET)
    line_str = line_str.replace('mh startup-delay','multihoming startup-delay')

  ### Bridge
  #### Bridge ports
  elif line_str.startswith('net add bridge bridge ports') or line_str.startswith('net del bridge bridge ports'):
    add = line_str.find('add') != -1
    line_str = line_str.replace('net add bridge bridge ports ', '')
    line_str = line_str.replace('net del bridge bridge ports ', '')
    line_str = line_str.replace('\n', '\n')
    temp_list = line_str.split(',')
    for word in temp_list:
      if (word.find('vni')==-1 and word.find('vxlan')==-1):
        word = word.replace('\n','')
        word = word.replace('-','_') # Added in 1.3.1 to handle '-' in interface names
        if add:
          line_str = NVUE_SET + 'interface ' + word + ' bridge domain br_default\n'
        else:
          line_str = NVUE_UNSET + 'interface ' + word + ' bridge\n'
        line_output.append(line_str)
        write_line = False
  #### Bridge vids
  elif line_str.startswith('net add bridge bridge vids'):
    temp_str = line_str.split()
    line_str = NVUE_SET + 'bridge domain br_default vlan ' + temp_str[-1] + '\n'
  #### Bridge pvid
  elif line_str.startswith('net add bridge bridge pvid'):
    temp_str = line_str.split()
    line_str = NVUE_SET + 'bridge domain br_default untagged ' + temp_str[-1] + '\n'
  #### Bridge vlan-aware
  elif line_str.startswith('net add bridge bridge vlan-aware'):
    line_str = NVUE_SET + 'bridge domain br_default type vlan-aware' + '\n'
  #### Bridge STP - updated in 1.5
  elif line_str.startswith('net add bridge stp treeprio'):
    line_str = line_str.replace('net add bridge stp treeprio', 'nv set bridge domain br_default stp priority')
  elif line_str.startswith('net add bridge stp off'):
    line_str = NVUE_SET + 'bridge domain br_default stp state down\n'
  #### Bridge ageing - Added support in CL 5.5
  elif line_str.startswith('net add bridge bridge ageing'):
    line_str = line_str.replace('net add bridge bridge ageing', 'nv set bridge domain br_default ageing')
  #### Bridge multicast - Added support in 1.5
  elif line_str.startswith('net add bridge bridge mcquerier'):
    line_str = NVUE_SET + 'bridge domain br_default multicast snooping querier enable on\n'
  elif line_str.startswith('net add bridge bridge mcsnoop'):
    line_str = NVUE_SET + 'bridge domain br_default multicast snooping enable ' + temp_list[-1] + '\n'
  #### Bridge untagged - Added support in 1.7
  elif line_str.startswith('net add bridge bridge allow-untagged'):
    line_str = NVUE_SET + 'bridge domain br_default untagged none\n'

  ### CLAG/MLAG
  #### net add clag peer macro
  elif line_str.startswith('net add clag peer sys-mac'):
    temp_str = line_str.split()
    priority = 1000 if temp_str[8] == 'primary' else 2000
    line_output.append(NVUE_SET + 'interface peerlink bond member ' + temp_str[7] + '\n')
    line_output.append(NVUE_SET + 'mlag mac-address ' + temp_str[5]  + '\n')
    if (len(temp_str) > 10):
      line_output.append(NVUE_SET + 'mlag backup ' + temp_str[10]  + '\n')
    line_output.append(NVUE_SET + 'mlag peer-ip linklocal' + '\n')
    line_output.append(NVUE_SET + 'mlag priority ' + str(priority) + '\n')
    write_line = False

  ### OSPF - Updated in 1.7
  elif line_str.startswith('net add ospf'):
    # line_str = line_str.replace('net add ospf', NVUE +' set router ospf')
    temp_list = line_str.split()
    if line_str.find(' vrf ') == -1:
      vrf_name = 'default'
    else:
      vrf_name = temp_list[temp_list.index('vrf') + 1]
    if line_str.find('router-id') != -1:
      line_str = line_str.replace('net add ospf', NVUE +' set router ospf')
    elif line_str.find('network') != -1 and line_str.find('area') != -1:
      line_str = NVUE_SET + 'vrf ' + vrf_name + ' router ospf area ' + temp_list[temp_list.index('area') + 1] + ' network ' + temp_list[temp_list.index('network') + 1] + '\n'
    elif line_str.find('reference-bandwidth') != -1:
      line_str = line_str.replace('net add ospf auto-cost reference-bandwidth', NVUE_SET +'vrf ' + vrf_name + ' router ospf reference-bandwidth')
    elif line_str.find('default-information') != -1:
      line_str = NVUE_SET + 'vrf ' + vrf_name + ' router ospf default-originate enable on\n'
      if line_str.find('always') != -1:
        line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf default-originate always on\n'
    elif line_str.find('log-adjacency-changes') != -1:
      if line_str.find('detail') != -1:
        line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf log adjacency-changes detail\n'
      else:
        line_str = NVUE_SET + 'vrf ' + vrf_name + ' router ospf log adjacency-changes on\n'
    elif line_str.find('redistribute') != -1:
      line_str = line_str.replace('net add ospf', NVUE_SET +'vrf ' + vrf_name + ' router ospf')
    elif line_str.find('nssa') != -1:
      if line_str.find('no-summary') != -1:
        line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf area ' + temp_list[temp_list.index('area') + 1] + ' type totally-nssa\n'
      else:
        line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf area ' + temp_list[temp_list.index('area') + 1] + ' type nssa\n'
    elif line_str.find('stub') != -1:
      if line_str.find('no-summary') != -1:
        line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf area ' + temp_list[temp_list.index('area') + 1] + ' type totally-stub\n'
      else:
        line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf area ' + temp_list[temp_list.index('area') + 1] + ' type stub\n'
    elif line_str.find('timers throttle') != -1:
      line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf timers spf delay ' + temp_list[-3] + '\n'
      line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf timers spf holdtime ' + temp_list[-2] + '\n'
      line_str = line_str +  NVUE_SET + 'vrf ' + vrf_name + ' router ospf timers spf max-holdtime ' + temp_list[-1] + '\n'
    else:
      line_str = "# MANUAL REVIEW - " + line_str + '\n'

  ### ACL - Added in 1.3.1
  elif line_str.startswith('net add acl'):
    temp_list = line_str.split()
    acl_type = temp_list[3]
    # acl_id = temp_list[4].replace('-','_') # Replace '-' with '_' in the name to avoid any errors in the naming in NVUE
    acl_id = temp_list[4] # '-' supported with CL 5.3 and above
    ## ACL priority number
    if line_str.find('priority') > 0:
      rule_id = temp_list[temp_list.index('priority') + 1]
    else:
      rule_id = '10'
    acl_str = ''
    
    ## ACL action - accept|drop|set-dscp|erspan|police|set-class|span destination
    if line_str.find(' accept ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action permit\n'
      ## ACL protocol - ospf|pim|icmp|igmp|tcp|udp
      if temp_list[temp_list.index('accept') + 1] in ['ospf','pim','icmp','igmp','tcp','udp','icmpv6']:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip protocol ' + temp_list[temp_list.index('accept') + 1] + '\n'
        acl_protocol = temp_list[temp_list.index('accept') + 1]
    if line_str.find(' drop ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action deny\n'
      ## ACL protocol - ospf|pim|icmp|igmp|tcp|udp
      if temp_list[temp_list.index('drop') + 1] in ['ospf','pim','icmp','igmp','tcp','udp','icmpv6']:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip protocol ' + temp_list[temp_list.index('drop') + 1] + '\n'
        acl_protocol = temp_list[temp_list.index('drop') + 1]
    if line_str.find(' set-dscp ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action set dscp ' + temp_list[temp_list.index('set-dscp') + 1] + '\n'
      ## ACL protocol - ospf|pim|icmp|igmp|tcp|udp
      if temp_list[temp_list.index('set-dscp') + 2] in ['ospf','pim','icmp','igmp','tcp','udp','icmpv6']:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip protocol ' + temp_list[temp_list.index('set-dscp') + 2] + '\n'
        acl_protocol = temp_list[temp_list.index('set-dscp') + 2]
    if line_str.find(' erspan ') > 0:
      # net add acl ipv4 (<acl-ipv4>|<text>) erspan source-ip <ipv4> dest-ip <ipv4> ttl <1-255> (ospf|pim|icmp|igmp|tcp|udp)
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action erspan source-ip ' + temp_list[temp_list.index('erspan') + 2] + '\n'
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action erspan dest-ip ' + temp_list[temp_list.index('erspan') + 4] + '\n'
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action erspan ttl ' + temp_list[temp_list.index('erspan') + 6] + '\n'
      ## ACL protocol - ospf|pim|icmp|igmp|tcp|udp
      if temp_list[temp_list.index('erspan') + 7] in ['ospf','pim','icmp','igmp','tcp','udp','icmpv6']:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip protocol ' + temp_list[temp_list.index('erspan') + 7] + '\n'
        acl_protocol = temp_list[temp_list.index('erspan') + 7]
      # adding logic to handle erspan source-ip/dest-ip like here - net add acl ipv4 (<acl-ipv4>|<text>) erspan source-ip <ipv4> dest-ip <ipv4> ttl <1-255> source-ip (any|<ipv4>|<ipv4/prefixlen>) dest-ip (any|<ipv4>|<ipv4/prefixlen>|local|iprouter)
      del temp_list[temp_list.index('erspan'):temp_list.index('erspan')+7]
      line_str = " ".join(temp_list)
    if line_str.find(' police ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action police mode ' + temp_list[temp_list.index('police') + 1] + '\n'
      ## ACL protocol - ospf|pim|icmp|igmp|tcp|udp
      if temp_list[temp_list.index('police') + 2] in ['ospf','pim','icmp','igmp','tcp','udp','icmpv6']:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip protocol ' + temp_list[temp_list.index('police') + 2] + '\n'
        acl_protocol = temp_list[temp_list.index('police') + 2]
    if line_str.find(' set-class ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action set class ' + temp_list[temp_list.index('set-class') + 1] + '\n'
      ## ACL protocol - ospf|pim|icmp|igmp|tcp|udp
      if temp_list[temp_list.index('set-class') + 2] in ['ospf','pim','icmp','igmp','tcp','udp','icmpv6']:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip protocol ' + temp_list[temp_list.index('set-class') + 2] + '\n'
        acl_protocol = temp_list[temp_list.index('set-class') + 2]
    if line_str.find(' span destination ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' action span ' + temp_list[temp_list.index('destination') + 1] + '\n'
      ## ACL protocol - ospf|pim|icmp|igmp|tcp|udp
      if temp_list[temp_list.index('destination') + 2] in ['ospf','pim','icmp','igmp','tcp','udp','icmpv6']:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip protocol ' + temp_list[temp_list.index('destination') + 2] + '\n'
        acl_protocol = temp_list[temp_list.index('destination') + 2] 
    
    ## ACL - source IP and dest IP
    if line_str.find(' source-ip ') > 0:
      if temp_list[temp_list.index('source-ip') + 1] == 'any':
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip source-ip ANY\n'
      else:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip source-ip ' + temp_list[temp_list.index('source-ip') + 1] + '\n'
    if line_str.find(' dest-ip ') > 0:
      if temp_list[temp_list.index('dest-ip') + 1] == 'any':
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip dest-ip ANY\n'
      else:
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip dest-ip ' + temp_list[temp_list.index('dest-ip') + 1] + '\n'
    
    ## ACL - source port and dest port - Updated in 1.7
    # As of CL 5.4, if the port is a range, we need to replace '-' with ':'. Eg: nv set acl webinar_vlan_out rule 510 match ip source-port 32768:60999
    # Convert 'any' to 'ANY'
    if line_str.find(' source-port ') > 0:
      if temp_list[temp_list.index('source-port') + 1] == 'any':
        # acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip source-port ANY\n'
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip ' + acl_protocol + ' source-port ANY\n'
      else:
        # acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip source-port ' + temp_list[temp_list.index('source-port') + 1].replace("-",":") + '\n' 
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip ' + acl_protocol + ' source-port ' + temp_list[temp_list.index('source-port') + 1].replace("-",":") + '\n' 
    if line_str.find(' dest-port ') > 0:
      if temp_list[temp_list.index('dest-port') + 1] == 'any':
        # acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip dest-port ANY\n'
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip ' + acl_protocol + ' dest-port ANY\n'
      else:
        # acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip dest-port ' + temp_list[temp_list.index('dest-port') + 1].replace("-",":") + '\n'
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip ' + acl_protocol + ' dest-port ' + temp_list[temp_list.index('dest-port') + 1].replace("-",":") + '\n'
    
    ## ACL - source mac and dest mac, protocol
    if line_str.find(' source-mac ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip source-mac ' + temp_list[temp_list.index('source-mac') + 1] + '\n'
    if line_str.find(' dest-mac ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip dest-mac ' + temp_list[temp_list.index('dest-mac') + 1] + '\n'
    if line_str.find(' protocol ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip protocol ' + temp_list[temp_list.index('protocol') + 1] + '\n'

    ## ACL - ICMP
    # Seen only in scenarios with inbound/outbound interface specified - Eg: net add acl ipv6 (<acl-ipv6>|<text>) set-class <0-7> icmp [router-solicitation|router-advertisement|neighbour-solicitation|neighbour-advertisement|<0-255>] (inbound-interface|outbound-interface) <interface>
    if line_str.find(' icmp ') > 0 and (line_str.find('inbound-interface') > 0 or line_str.find('outbound-interface') > 0):
      if acl_type == 'ipv4':
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip icmp-type ' + temp_list[temp_list.index('icmp') + 1] + '\n'
      elif acl_type == 'ipv6':
        acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip icmpv6-type ' + temp_list[temp_list.index('icmp') + 2] + '\n'
    
    ## ACL - TCP flags
    if line_str.find(' tcp-flags ') > 0:
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip tcp flags ' + temp_list[temp_list.index('tcp-flags') + 1] + '\n'
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip tcp mask ' + temp_list[temp_list.index('tcp-flags') + 2] + '\n'
    
    ## ACL - dscp
    if temp_list[-4] == 'dscp':
      # using temp_list[-4] instead of find('dscp') to catch the dscp action like here - net add acl ipv4 (<acl-ipv4>|<text>) dscp set-dscp <dscp> (ospf|pim|icmp|igmp|tcp|udp) dscp <dscp> (inbound-interface|outbound-interface) <interface>
      acl_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' rule ' + rule_id + ' match ip dscp ' + temp_list[-3] + '\n'
    
    ## ACL type - ipv4|ipv6
    line_str = acl_str + NVUE_SET + 'acl ' + acl_id + ' type ' + acl_type + '\n'
  
  ### Move the rest of unhandled net del into unsupported list
  if line_str.startswith('net del'):
    line_str = '# SCRIPT UNSUPPORTED - ' + orig_str

  ### This is the catch all, any remaining `net add` was unhandled
  if line_str.startswith('net add '):
    line_str = line_str.replace('net add ', '# MANUAL REVIEW - net add ')

  if write_line:
    if line_str[-1] != "\n":
      line_str = line_str + "\n"
    line_output.append(line_str)
    if DEBUG:
      print('DEBUG orig_str1: ' + orig_str)
      print('DEBUG line_str2: ' + line_str)
      debug_output.append(orig_str + '-->\n\t' + line_str + '\n')
  else:
    write_line = True


# Create new VNI config
for k, v in VNI_STRUCT.items():
  # only create VNI mapping for L2 VNI's, excluding L3 VNIs (configured automatically)
  if 'vni' in k and v['vni'] and v['vlan'] and (not v['vlan'] in L3_VNIs):
    line_str = NVUE_SET + 'bridge domain br_default vlan ' + v['vlan'] + ' vni ' + v['vni'] + '\n'
    line_output.append(line_str)

#dump link state
for k, v in INTERFACE_LINK_STATES.items():
  line_str = NVUE_SET + 'interface ' + k + ' link state ' + v + '\n'
  line_output.append(line_str)



# remove duplicated from list
#print(line_output)
line_output_filtered = []
for line in line_output:
  if (line not in line_output_filtered) and (line.startswith('#') or line.startswith('nv')):
    line_output_filtered.append(line)

line_output_filtered.append('\n\n')
line_output_filtered.append('# EXECUTE MANUALLY: APPLY THE CONFIG - ' + NVUE + ' config apply' + '\n')
line_output_filtered.append('# EXECUTE MANUALLY: SAVE  THE CONFIG - ' + NVUE + ' config save' + '\n')

for i in line_output_filtered:                # go through line by line of the input file
   output_file.write(i)              # write new output file
   #print(i)                     # dump it on the screen too
input_file.close()                  # close the input file
output_file.close()                 # close the output file
if DEBUG:
  for i in debug_output:
    debug_file.write(i)
  debug_file.close()

print ('Convert NCLU to NVUE script - version '+current_ver)
print('Using output type',NVUE)
print ('Output file is :', outputfile)
