default: test

test-acl:
	python3 ./convert.py -i tests/acl/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/acl/output
	@echo 'ACL test passed'

test-adg1:
	python3 ./convert.py -i tests/adg1/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/adg1/output
	@echo 'ADG1 test passed'

test-case1:
	python3 ./convert.py -i tests/case1/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/case1/output
	@echo 'CASE1 test passed'

test-cl51:
	python3 ./convert.py -i tests/cl51/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/cl51/output
	@echo 'CL51 test passed'

test-cl52:
	python3 ./convert.py -i tests/cl52/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/cl52/output
	@echo 'CL52 test passed'

test-evpn-mh-pim:
	python3 ./convert.py -i tests/evpn-mh-pim/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/evpn-mh-pim/output
	@echo 'EVPN MH PIM test passed'

test-jdh1:
	python3 ./convert.py -i tests/jdh1/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/jdh1/output
	@echo 'JDH1 test passed'

test-jdh2:
	python3 ./convert.py -i tests/jdh2/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/jdh2/output
	@echo 'JDH2 test passed'

test-leftovers:
	python3 ./convert.py -i tests/leftovers/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/leftovers/output
	@echo 'leftovers test passed'

test-mlag:
	python3 ./convert.py -i tests/mlag-symm/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/mlag-symm/output
	@echo 'MLAG test passed'

test-zoho1:
	python3 ./convert.py -i tests/zoho1/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/zoho1/output
	@echo 'ZOHO1 test passed'

test-zoho2:
	python3 ./convert.py -i tests/zoho2/input
	diff --ignore-blank-lines --ignore-all-space output.txt tests/zoho2/output
	@echo 'ZOHO2 test passed'

test: test-acl test-adg1 test-case1 test-cl51 test-cl52 test-evpn-mh-pim test-jdh1 test-jdh2 test-leftovers test-mlag test-zoho1 test-zoho2

update-test-results:
	python3 ./convert.py -i tests/acl/input -o tests/acl/output
	python3 ./convert.py -i tests/adg1/input -o tests/adg1/output
	python3 ./convert.py -i tests/case1/input -o tests/case1/output
	python3 ./convert.py -i tests/cl51/input -o tests/cl51/output
	python3 ./convert.py -i tests/cl52/input -o tests/cl52/output
	python3 ./convert.py -i tests/jdh1/input -o tests/jdh1/output
	python3 ./convert.py -i tests/jdh2/input -o tests/jdh2/output
	python3 ./convert.py -i tests/evpn-mh-pim/input -o tests/evpn-mh-pim/output
	python3 ./convert.py -i tests/leftovers/input -o tests/leftovers/output
	python3 ./convert.py -i tests/mlag-symm/input -o tests/mlag-symm/output
	python3 ./convert.py -i tests/zoho1/input -o tests/zoho1/output
	python3 ./convert.py -i tests/zoho2/input -o tests/zoho2/output